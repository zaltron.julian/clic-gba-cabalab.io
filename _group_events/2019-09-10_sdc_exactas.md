---
title: Semana de la Computación
date: 2019-09-10
image: /images/sdc-exactas-2020/sdc-ilu.jpg
---

Charla "Programando sonidos e imágenes" por Iris Saladino y Diego Dorado, donde vieron cómo se puede trabajar con expresiones artísticas de la imagen y el sonido por medio de programación. La semana finalizó con una perfo de CLiC donde participaron: Rafaela Correa Marjak, Mathi Gatti, Sol Sarratea y pablito. Código disponible en: [https://github.com/bu3nAmigue/livecoding-tools/tree/master/songs/tedX](https://github.com/bu3nAmigue/livecoding-tools/tree/master/songs/tedX)
