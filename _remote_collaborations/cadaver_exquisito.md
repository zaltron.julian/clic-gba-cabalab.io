---
title: Cadaver exquisito
image: /images/cadaver/irisS_vlad.png
---

Scripts colectivos logrados con el intercambio de líneas de código a través
de mensajería instantánea. Por turno, cada coder propone una nueva línea
considerando la que recibió de su colega.
